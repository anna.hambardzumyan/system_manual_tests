
## Test Case ID 004
## Test Case Name
Verify the new created event  in Demio is visible  on Boost active events page with the label "Standard". 

## Pre-conditions
Make sure the user signed in Demio
## Steps To Reproduce
1. CLick on "Events" button
2. Click on "Add new event"  button
3. Select the type of the event
4. Enter a Name of the Event
5. Click on the button "Continue"
6. CLick on the button "Boost Registrations"
  
## Expected Result
 The created event is displayed on the  Boost page

-------------------------------------------------------------------------------------------

## Test Case ID 005

## Test Case Name
Verify the new created  Standard event  in Demio is visible in  the Standards' event section of the Boost page

## Pre-conditions
Make sure the user signed in Demio
## Steps To Reproduce
1. CLick on "Events" button
2. Click on "Add new event"  button
3. Select the "Standard event"
4. Enter a Name of the Event
5. Click on the button "Continue"
6. CLick on the button "Boost Registrations"
   
## Expected Result
The created Standard event is displayed on the Boost page under the Standard section
 
-----------------------------------------------------------------------------------

## Test Case ID 005
## Test Case Name
Verify the new created  Series event  in Demio is visible in  the Series' event section of the Boost page

## Pre-conditions
Make sure the user signed in Demio
## Steps To Reproduce
1. CLick on "Events" button
2. Click on "Add new event"  button
3. Select the "Series" event
4. Enter a Name of the Event
5. Click on the button "Continue"
6. CLick on the button "Boost Registrations"
   
## Expected Result 
The created  Series event is displayed on the  Boost page under the Series section

 ---------------------------------------------------------------------------------------------
## Test Case ID 006
## Test Case Name
Verify the new created  Automated event  in Demio is visible in  the Automated event section of the Boost page

## Pre-conditions
Make sure the user signed in Demio
## Steps To Reproduce
1. CLick on "Events" button
2. Click on "Add new event"  button
3. Select the "Automated" event
4. Enter a Name of the Event
5. Click on the button "Continue"
6. CLick on the button "Boost Registrations"
   
## Expected Result 
The created Automated event is displayed in Boost page under the Series section

----------------------------------------------------------------------------------------------------
## Test Case ID 007
## Test Case Name
Verify the name of the Standard event is updating in the "Boost" page after editing in Demio
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Standard event in Demio
## Steps To Reproduce
1. CLick on  the Standard Event 
2. Click on the "edit" icon near the name of the event
3. Edit the name of the event
4. Click on the "Save" button
5. Click on the  button "Boost Registrations" 
## Expected Result 
The name of the Standard event is updated in the Boost page

----------------------------------------------------------------------------------------
## Test Case ID 008
## Test Case Name
Verify the name of the Series  event is updating in the "Boost" page after editing in Demio
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Series event in Demio
## Steps To Reproduce
1. CLick on  the Series Event 
2. Click on the "edit" icon near the name of the event
3. Edit the name of the event
4. Click on the "Save" button
5. Click on the  button "Boost Registrations" 
## Expected Result 
The name of the Series event is updated in the Boost page

--------------------------------------------------------------------------------
## Test Case ID 009
## Test Case Name
Verify the name of the Automated event is updating in the "Boost" page after editing in Demio
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Automated  event in Demio
## Steps To Reproduce
1. CLick on  the Automated Event 
2. Click on the "edit" icon near the name of the event
3. Edit the name of the event
4. Click on the "Save" button
5. Click on the  button "Boost Registrations" 
## Expected Result 

The name of the Automated event is updated in the Boost page

-------------------------------------------------------------------------------------
## Test Case ID 010
## Test Case Name
Verify that duplicated  Standard event is displayed in the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Standard event in Demio
## Steps To Reproduce
1. CLick on  the drop-down near the name of the event
2. From the drop-down list select "Duplicate"

## Expected Result 

The duplicated Standard event is displayed in the Boost page

-------------------------------------------------------------------------
## Test Case ID 011
## Test Case Name
Verify that duplicated  Series event is displayed in the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Series event in Demio
## Steps To Reproduce
1. CLick on  the  down near the name of the event
2. From the drop-down list select "Duplicate"drop

## Expected Result 

The duplicated Series event is displayed on the Boost page

-------------------------------------------------------------------------
# Test Case ID 012
## Test Case Name
Verify that duplicated  Automated event is displayed in the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Automated event in Demio
## Steps To Reproduce
1. CLick on  the  drop-down near the name of the event
2. From the drop-down list select "Duplicate"drop
## Expected Result 

The duplicated Automated event is displayed on the Boost page

-------------------------------------------------------------------------
## Test Case ID 013
## Test Case Name
Verify that archived Standard event is displayed in the Archived section of the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Standard event in Demio
## Steps To Reproduce
1. CLick on  the drop-down near the name of the event
2. From the drop-down list select "Archive"

## Expected Result 

The archived Standard event is displayed in the Archived section of the Boost page

-------------------------------------------------------------------------
## Test Case ID 014
## Test Case Name
Verify that archived Series event is displayed in the Archived section of the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Standard event in Demio
## Steps To Reproduce
1. CLick on  the drop-down near the name of the event
2. From the drop-down list select "Archive"

## Expected Result 

The archived Standard event is displayed in the Archived section of the Boost page

------------------------------------------------------------------------------------
## Test Case ID 015
## Test Case Name
Verify that archived Automated event is displayed in the Archived section of the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created Standard event in Demio
## Steps To Reproduce
1. CLick on  the drop-down near the name of the event
2. From the drop-down list select "Archive"

## Expected Result 

The archived Standard event is displayed in the Archived section of the Boost page


## Test Case ID 016
## Test Case Name
Verify that unarchived Standard event is displayed in the Events section of the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created and archived Standard event in Demio
## Steps To Reproduce
1. Click on the tab "Archived"
2. CLick on  button "Unarchive"
3. Click on the button "Boost registration"

## Expected Result 

The unarchived  Standard event is displayed in the Events section of the Boost page

-------------------------------------------------------------------------------------

## Test Case ID 017
## Test Case Name
Verify that unarchived Series event is displayed in the Events section of the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created and archived Series event in Demio
## Steps To Reproduce
1. Click on the tab "Archived"
2. CLick on  button "Unarchive"
3. Click on the button "Boost registration"

## Expected Result 

The unarchived  Series event is displayed in the Events section of the Boost page

-------------------------------------------------------------------------------------

## Test Case ID 018
## Test Case Name
Verify that unarchived Automated event is displayed in the Events section of the Boost page
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created and archived Automated event in Demio
## Steps To Reproduce
1. Click on the tab "Archived"
2. CLick on  button "Unarchive"
3. Click on the button "Boost registration"

## Expected Result 

The unarchived  Automated event is displayed in the Events section of the Boost page

----------------------------------------------------------------------------

## Test Case ID 019
## Test Case Name
Verify  the deleted event in Demio is not displayed on the Boost page 
## Pre-conditions 
Make sure the user signed in Demio
Make sure there is a created and archived event in Demio
## Steps To Reproduce
1. Click on the tab "Archived"
2. CLick on  the "Delete" icon

## Expected Result 

The event should not be displayed in the Archived section of the Boost page

-------------------------------------------------------------------------------------

## Test Case ID 020
## Test Case Name
Verify that after creating sessions  for the Standard events the registrants in Demio are displayed on the  Boost page
## Pre-conditions 

Make sure the user  is signed in Demio

Make sure there is a created Standard event

Make sure the event has sessions

Make  sure there is a registrant for the current session

## Steps To Reproduce
1. Click on the button "Boost registration"
2. Click on the event
3. Click on the tab "Audience"
4. Check the registrant's name and the email in the Promoter's  section

## Expected Result 
The registrant's name and the email are displayed in promoter's section

------------------------------------------------------------------------------------
## Test Case ID 021
## Test Case Name
Verify that after creating sessions for the Series event the registrants in Demio are displayed on the  Boost page
## Pre-conditions 
Make sure the user  is signed in Demio

Make sure there is a created event

Make sure the event has sessions

Make  sure there is a registrant for the event

## Steps To Reproduce
1. Click on the button "Boost registration"
2. Click on the event
3. Click on the tab "Audience"
4. Check the registrant's name and  email in the  promoter's  section

## Expected Result 
The registrant's name and the email are displayed in the promoter's section

-------------------------------------------------------------------------------------
## Test Case ID 022
## Test Case Name
Verify that after creating sessions  for the Automated event the registrants in Demio are displayed on the  Boost page
## Pre-conditions 
Make sure the user  is signed in Demio.<br>
Make sure there is a created event.<br>
Make sure the event has sessions.<br>
Make  sure there is a registrant for the event.<br>

## Steps To Reproduce
1. Click on the button "Boost registration"
2. Click on the event
3. Click on the tab "Audience"
4. Check the registrant's name and  email in the  promoter's  section

## Expected Result 
The registrant's name and the email are displayed in the promoter's section

-------------------------------------------------------------------------------
## Test Case ID 023
## Test Case Name
Verify the registrations on the scheduled sessions for the Standard event are displayed on the   Boost page
## Pre-conditions 
Make sure the user  is signed in Demio.
Make sure there is a created  Standard event.
Make sure the event has  scheduled sessions.
Make  sure there is a registrant for the event
## Steps To Reproduce
1. Click on the button "Boost registration"
2. Click on the event
3. Click on the tab "Audience"
4. Check the registrant's name and  email in the  promoter's  section

## Expected Result 
The registrant's name and the email are displayed in the promoter's section

---------------------------------------------------------------------------------
## Test Case ID 024
## Test Case Name
Verify the registrants on the scheduled sessions  for the Series event are displayed on the   Boost page
## Pre-conditions 
Make sure the user  is signed in Demio

Make sure there is a created event

Make sure the event has scheduled sessions with different dates
Make sure there are registrants on all the scheduled sessions

## Steps To Reproduce
1. Click on the button "Boost registration"
2. Click on the event
3. Click on the tab "Audience"
4. Check the registrants' names and  email in the  promoter's  section

## Expected Result 
All the registrants from all the sessions should be displayed on the Boost page

------------------------------------------------------------------------------------
## Test Case ID 025
## Test Case Name
Verify the registrants on the scheduled sessions  for the Series event are displayed on the   Boost page
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event
Make sure the event has scheduled sessions with different dates
Make sure there are registrants on all the scheduled sessions

## Steps To Reproduce
1. Click on the button "Boost registration"
2. Click on the event
3. Click on the tab "Audience"
4. Check the registrants' names and  email in the  promoter's  section

## Expected Result 
All the registrants from all the sessions should be displayed on the Boost page
--------------------------------------------------------------------------------
## Test Case ID 030
## Test Case Name
Verify the registrants from shared events are displayed on the Boost page
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with the sessions

## Steps To Reproduce

1. Click on the created event
2. Click on the share button
3. Share the event on the FB, Twitter or LinkedIn
4. Open FB page and click on the shared event
5. Do registration
6. Click on the Boost registration button
7. Click on the tab "Audience"
8. Check the registrant's name and  email in the  promoter's  section

## Expected Result 
The  registrants from the  shared events are displayed on the Boost page

----------------------------------------------------------------------------------
## Test Case ID 031
## Test Case
Verify  that ten  standard created events are synced and displayed on the Boost page
## Pre-conditions 
Make sure there is a created new account
Make sure the user  is signed in to Demio
Make sure there are created  ten Standard events
Make sure it is the initial sync

## Steps To Reproduce

1. Click on the "Boost registrations" button
2. Verify the count of the events

## Expected Result
Ten standard events are displayed on the Boost page


----------------------------------------------------------------------------------
## Test Case ID 032
## Test Case
Verify  that ten  Series created events are synced and displayed on the Boost page
## Pre-conditions 
Make sure there is a created new account
Make sure the user  is signed in to Demio
Make sure there are created  ten Series events
Make sure it is the initial sync

## Steps To Reproduce

1. Click on the "Boost registrations" button
2. Verify the count of the events

## Expected Result
Ten Series events are displayed on the Boost page


----------------------------------------------------------------------------
## Test Case ID 033
## Test Case
Verify  that ten  Automated created events are synced and displayed on the Boost page
## Pre-conditions 
Make sure there is a created new account
Make sure the user  is signed in to Demio
Make sure there are created  ten Automated events
Make sure it is the initial sync

## Steps To Reproduce

1. Click on the "Boost registrations" button
2. Verify the count of the events

## Expected Result
Ten Automated events are displayed on the Boost page

---------------------------------------------------------------------------------
## Test Case ID 034
## Test Case
Verify  that more than ten created events are displayed on the Boost page
## Pre-conditions 
Make sure there is a created new account
Make sure the user  is signed in to Demio
Make sure there are created more than ten Standard, Series and Automated events
Make sure it is the initial sync

## Steps To Reproduce

1. Click on the "Boost registrations" button
2. Verify the count of the events

## Expected Result
All created events are displayed on th Boost page
 
---------------------------------------------------------------------------------
## Test Case ID 034
## Test Case
Verify  the logo is displayed on the Email template during initial sync
## Pre-conditions 
Make sure there is a created new account
Make sure the user  is signed in to Demio
Make sure there is a new created Event
Make sure there is added company logo from the  branding section
Make sure it is the initial sync

## Steps To Reproduce

1. Click on the "Boost registrations" button
2. Click on the "Customize" button
3. Click on the Email section
4. Click on the Edit button
5. Check the Email template

## Expected Result
The company logo shuld be displayed on the Email template
 
---------------------------------------------------------------------------------