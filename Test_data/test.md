## ** Test Case ID 001**

## Test Case Name 
* Verify the user is logged in to "Boost"  page  after signing in  DEMIO()

## Pre-conditions
Make sure  the user is registered in Demio  
Make sure the user is signed in Demio account

## Steps To Reproduce
1. Click on the "Boost Registration" button and check the state of the user
 ## Expected Result
* The user is logged in

--------------------------------------------
## **Test Case ID --002**

## Test Case Name 
Verify the user navigates to Demio's login page after logging out in "Boost"

## Pre-conditions
* Make sure the user is signed in Demio page


## Steps To Reproduce

1.Click on the "Boost registration" button

2.From the Profile section click on the  "Logout" button
   
## Expected Result
* The user is logged out and navigated to Demio's login page
---------------------------------------------------------
## Test Case ID-003

## Test Case Name
* Verify the user is able to sign in from the via link: https://boost.banzai.io

## Pre-conditions

## Steps To Reproduce

 1.Navigate by  the via link: https://boost.banzai.io/boost

 2.Fill the credentials in redirected Demio's login page

 3. Click on "Login" button

 4. Click on the "Boost registration" button and check the state of the user

## Expected Result
The user is signed in  the "Boost" page