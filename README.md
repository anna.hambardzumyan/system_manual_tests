This README.md file marks out system_manual_tests which includes [Test case example](test_case_example.md).
Thus, It would be utilized to be as

* [Live_sync_flow tests](live_sync_flow_tests.md),
* [Audience](audience_tests.md),
* [Login_flow_tests](login_flow_tests.md),

All the necessary test data and related files for using tests are located in  [Test_data](./Test_data/) folder.
During usage please do not modify the original ones and do not change the structure.

## Test Case Specification Identifiers are:
**Note:** Test Case Specification defines the exact set up and inputs for 
one Test Case

* Test case ID
* Test Case Name
* Test Case Summary
* Pre-conditions
* Steps To Reproduce
* Expected Result
* Attachments


 **The following credentials were used for testing in the Dev environment.** <br>
   Here is the link to navigate to Dev environment: https://my.dev.demio.com/login <br>
<details>
<summary>E-mail:</summary>
audience@gmail.com 
</details> 
<details> 
<summary> Password: </summary> 
 Password1234
 </details> 

# Project Description

**Boost** is a product built for event marketers to increase the number of registrations with a word-of-mouth marketing strategy.

### How to access it?

The only way to access the Boost interface is from Demio.
[Click here to navigate to  Demio.com](https://www.demio.com/)

In the header, there is a tab called Boost.
![](./Test_data/Images/boost.png "Text to show on mouseover")<br>
By clicking on it there can be two scenarios of how the user accesses Boost: <br>

1. When users are visiting Boost first time they are directed to the specific landing page where 
there is information about the product. Depends on the screen size the “Start trial” button appears.
By clicking on that button users are being directed to the bottom of the page where there is a button to start trial.
Here, the users should click on Start Trial if they want to continue the process.

![](./Test_data/Images/landing.png)<br>
2.When users are not new to Boost they easily access the interface without interruptions.
### Main Page

The first main page shows all the Demio active events. The list includes the following data about the events: Title, Type, information about sessions, Status (color dots) and button to manage the event in Boost.
![](./Test_data/Images/Boost1.png "Text to show on mouseover")

